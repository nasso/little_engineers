local Settings = require "assets.settings"

function love.conf(t)
    t.identity = "coolest_game"

    t.window.title = "Cool Game"
    t.window.width = 1280
    t.window.height = 960
    t.window.resizable = true
end

local utils = {}

function utils.contains(t, val)
    return utils.key_of(t, val) ~= nil
end

function utils.icontains(t, val)
    return utils.index_of(t, val) ~= nil
end

function utils.key_of(t, val)
    for k, v in pairs(t) do
        if v == val then return k end
    end

    return nil
end

function utils.index_of(t, val)
    for i, v in ipairs(t) do
        if v == val then return i end
    end

    return nil
end

function utils.remove_val(t, val)
    local k = utils.key_of(t, val)

    if k ~= nil then table.remove(t, k) end
end

function utils.remove_ival(t, val)
    local i = utils.index_of(t, val)

    if i ~= nil then table.remove(t, i) end
end

function utils.first_of(t, ...)
    local vals = { ... }

    for _, v in pairs(t) do
        if utils.icontains(vals, v) then
            return v
        end
    end

    return nil
end

function utils.ifirst_of(t, ...)
    local vals = { ... }

    for _, v in ipairs(t) do
        if utils.icontains(vals, v) then
            return v
        end
    end

    return nil
end

function utils.discard_self_arg(fn)
    return function(...)
        return fn(nil, ...)
    end
end

function utils.copy(t, deep)
    deep = deep or false

    if type(t) == "table" then
        local cp = {}

        for k, v in pairs(t) do
            if deep then
                cp[k] = utils.copy(v, true)
            else
                cp[k] = v
            end
        end

        return cp
    end

    return t
end

return utils

local Class = require "hump.class"
local utils = require "utils"

local Observable = Class {}

function Observable:trigger_event(...)
    if self.event_listeners ~= nil then
        for i = 1, #(self.event_listeners) do
            self.event_listeners[i](self, ...)
        end
    end
end

function Observable:add_event_listener(l)
    if self.event_listeners == nil then
        self.event_listeners = {}
    end

    table.insert(self.event_listeners, l)
end

function Observable:remove_event_listener(l)
    if self.event_listeners ~= nil then
        utils.remove_ival(self.event_listeners, l)
    end
end

return Observable

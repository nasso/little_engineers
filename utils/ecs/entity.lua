local Class = require "hump.class"
local Observable = require "utils.observable"

local Entity = Class {}
Entity:include(Observable)

Entity.EVENT_COMPONENT_ADDED = "component_added"
Entity.EVENT_COMPONENT_REMOVED = "component_removed"

function Entity:init(name)
    self.name = name
end

function Entity:add(component, ...)
    self[component.compid] = component
    self:trigger_event(Entity.EVENT_COMPONENT_ADDED, component)

    -- tail-call!!
    if ... then
        return self:add(...)
    end
end

function Entity:remove(component, ...)
    if self:has(component) then
        self[component.compid] = nil
        self:trigger_event(Entity.EVENT_COMPONENT_REMOVED, component)
    end

    -- tail-call!!
    if ... then
        return self:remove(...)
    end
end

function Entity:get(componentClass)
    return self[componentClass.compid]
end

function Entity:has(componentClass)
    return self:get(componentClass) ~= nil
end

function Entity:__tostring()
    return "[Entity: " .. self.name .. "]"
end

return Entity

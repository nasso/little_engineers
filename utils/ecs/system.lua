local Class = require "hump.class"

local System = Class {}

-- Filter helpers
-- Filters out any entity not having ALL of the components
function System.require_all(...)
    local all = { ... }

    return function(e)
        for i = 1, #all do
            local p = all[i]

            -- Another filtering function
            if type(p) == "function" then
                -- Return as soon as one doesn't match (require_all)
                if not p(e) then
                    return false
                end

            -- Assume anything else is a component class
            else
                -- Return as soon as one doesn't match (require_all)
                if not e:has(p) then
                    return false
                end
            end
        end

        return true
    end
end

-- Filters out any entity not having ANY of the components
function System.require_any(...)
    local all = { ... }

    return function(e)
        for i = 1, #all do
            local p = all[i]

            -- Another filtering function
            if type(p) == "function" then
                -- Return as soon as one matches (require_any)
                if p(e) then
                    return true
                end

            -- Assume anything else is a component class
            else
                -- Return as soon as one matches (require_any)
                if e:has(p) then
                    return true
                end
            end
        end

        return false
    end
end

-- Filters out any entity having ALL of the components
function System.reject_all(...)
    -- Is the opposite of require_all
    local p = System.require_all(...)

    return function(e)
        return not p(e)
    end
end

-- Filters out any entity having ANY of the components
function System.reject_any(...)
    local p = System.require_any(...)

    return function(e)
        return not p(e)
    end
end

-- Helper to create a new system
function System.new()
    return Class { __includes = System }
end

-- Default entity filter
function System:filter(e)
    return true
end

-- Callbacks
-- function System:on_add_to_world(w) end
-- function System:on_remove_from_world(w) end
-- function System:on_update(w, dt) end
-- function System:on_draw(w) end

return System

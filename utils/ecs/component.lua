local Class = require "hump.class"

local comp_counter = 0
local tags = {}

local Component = {}

function Component.new()
    comp_counter = comp_counter + 1
    return Class { compid = "comp_" .. string.format('%x', comp_counter)  }
end

function Component.tag(name)
    if not tags[name] then
        tags[name] = { compid = "tag_" .. name  }
    end

    return tags[name]
end

return Component

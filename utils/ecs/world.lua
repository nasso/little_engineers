local Class = require "hump.class"
local Entity = require "utils.ecs.entity"
local utils = require "utils"

local World = Class {}

-- Constructor
function World:init()
    self.auto_refresh = true
    self.resources = {}
    self.entities = {}
    self.systems = {}

    self.on_entity_event = function(e)
        self:_auto_refresh_all_systems_with_entity(e)
    end
end

-- Add an entity
function World:add_entity(e)
    table.insert(self.entities, e)
    e:add_event_listener(self.on_entity_event)

    self:_auto_refresh_all_systems_with_entity(e)
end

-- Add a system
function World:add_system(s)
    table.insert(self.systems, s)

    if s.on_add_to_world then
        s:on_add_to_world(self)
    end

    self:_auto_refresh_system_with_all_entities(s)
end

-- Remove the entity at the specified index
function World:remove_entity_at(i)
    local e = table.remove(self.entities, i)

    if e then
        e:remove_event_listener(self.on_entity_event)
    end

    self:_auto_refresh_all_systems_with_entity(e)

    return e
end

-- Remove the system at the specified index
function World:remove_system_at(i)
    local sys = table.remove(self.systems, i)

    if sys then
        if sys.on_remove_from_world then
            sys:on_remove_from_world(self)
        end

        return sys
    end
end

-- Refresh specified system entity cache against the specified entity
function World:refresh_system_with_entity(s, e)
    -- Create the entity table if it doesn't exist already
    if not s.entities then
        s.entities = {}
    end

    -- Current state
    local should_have = utils.icontains(self.entities, e) and s.filter(e)
    local index = utils.index_of(s.entities, e)
    local actually_has = index ~= nil

    -- Check for new stuff
    if should_have ~= actually_has then
        if should_have then
            table.insert(s.entities, e)
        else
            table.remove(s.entities, index)
        end
    end
end

-- Update
function World:update(dt)
    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_pre_update then
            sys:on_pre_update(self, dt)
        end
    end

    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_update then
            sys:on_update(self, dt)
        end
    end

    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_post_update then
            sys:on_post_update(self, dt)
        end
    end
end

-- Draw
function World:draw()
    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_pre_draw then
            sys:on_pre_draw(self)
        end
    end

    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_draw then
            sys:on_draw(self)
        end
    end

    for i = 1, #(self.systems) do
        local sys = self.systems[i]

        if sys.on_post_draw then
            sys:on_post_draw(self)
        end
    end
end

-- EVERY FOLLOWING METHOD IS JUST AN INDIRECT CALL TO THE METHODS ABOVE

-- Remove the entity
function World:remove_entity(e)
    local i = utils.index_of(self.entities, e)

    if i then
        return self:remove_entity_at(i)
    end
end

-- Remove the system
function World:remove_system(s)
    local i = utils.index_of(self.systems, s)

    if i then
        return self:remove_system_at(i)
    end
end

-- Remove all entities
function World:clear_entities()
    while #(self.entities) ~= 0 do
        self:remove_entity_at(1)
    end
end

-- Remove all systems
function World:clear_systems()
    while #(self.systems) ~= 0 do
        self:remove_system_at(1)
    end
end

-- Refresh entity cache for all systems against the specified entity
function World:refresh_all_systems_with_entity(e)
    for i = 1, #(self.systems) do
        self:refresh_system_with_entity(self.systems[i], e)
    end
end

-- Refresh entity cache for this system against all the entities
function World:refresh_system_with_all_entities(s)
    for i = 1, #(self.entities) do
        self:refresh_system_with_entity(s, self.entities[i])
    end
end

-- Refresh entity cache for all systems against all entities
function World:refresh_all_systems_with_all_entities()
    for i = 1, #(self.systems) do
        self:refresh_system_with_all_entities(self.systems[i])
    end
end

-- Automatic version of World:refresh_system_with_entity
function World:_auto_refresh_system_with_entity(...)
    if self.auto_refresh then
        self:refresh_system_with_entity(...)
    end
end

-- Automatic version of World:refresh_all_systems_with_entity
function World:_auto_refresh_all_systems_with_entity(...)
    if self.auto_refresh then
        self:refresh_all_systems_with_entity(...)
    end
end

-- Automatic version of World:refresh_system_with_all_entities
function World:_auto_refresh_system_with_all_entities(...)
    if self.auto_refresh then
        self:refresh_system_with_all_entities(...)
    end
end

-- Automatic version of World:refresh_all_systems_with_all_entities
function World:_auto_refresh_all_systems_with_all_entities(...)
    if self.auto_refresh then
        self:refresh_all_systems_with_all_entities(...)
    end
end

-- Varargs version
function World:add_entities(entities)
    for i = 1, #entities do
        self:add_entity(entities[i])
    end
end

-- Varargs version
function World:add_systems(systems)
    for i = 1, #systems do
        self:add_system(systems[i])
    end
end

-- Varargs version
function World:remove_entities(entities)
    for i = 1, #entities do
        self:remove_entity(entities[i])
    end
end

-- Varargs version
function World:remove_systems(systems)
    for i = 1, #systems do
        self:remove_system(systems[i])
    end
end

return World

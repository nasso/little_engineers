local Class = require "hump.class"

-- https://github.com/stevedonovan/Penlight/blob/master/lua/pl/path.lua#L286
local function format_path(path)
    local np_gen1, np_gen2  = '[^SEP]+SEP%.%.SEP?', 'SEP+%.?SEP'
    local np_pat1, np_pat2 = np_gen1:gsub('SEP','/'), np_gen2:gsub('SEP','/')
    local k

    repeat -- /./ -> /
        path, k = path:gsub(np_pat2, '/')
    until k == 0

    repeat -- A/../ -> (empty)
        path, k = path:gsub(np_pat1, '')
    until k == 0

    if path == '' then path = '.' end

    return path
end

local Tileset = Class {}

function Tileset.load(set)
    local dir = ""
    local data = nil

    -- get data
    if type(set) == "table" then
        data = set
    else
        -- Ext check
        local ext = set:sub(-4, -1)
        assert(ext == ".lua", string.format(
            "Invalid file type: %s. File must be of type: lua.",
            ext
        ))

        -- Get directory
        dir = set:reverse():find("[/\\]") or ""
        if dir ~= "" then
            dir = set:sub(1, 1 + (#set - dir))
        end

        -- Load set
        data = assert(love.filesystem.load(set))()
    end

    -- tell em we only support tile atlases
    assert(data.image, data.name .. " should be a Tile Atlas.")

    local animations = nil

    -- load animations
    for i = 1, #data.tiles do
        local anim = data.tiles[i].animation

        if type(anim) == "table" then
            if animations == nil then
                animations = {}
            end

            table.insert(animations, anim)
        end
    end

    return Tileset(
        format_path(dir .. data.image),
        data.columns,
        data.tilecount / data.columns,
        data.margin,
        data.spacing,
        animations
    )
end

function Tileset:init(image, columns, rows, margin, spacing, animations)
    assert(image, "image is nil")
    assert(type(columns) == "number", "columns must be a number, got " .. type(columns))
    assert(type(rows) == "number", "rows must be a number, got " .. type(rows))
    assert(
        not animations or type(animations) == "table",
        "animations must be a table, got " .. type(animations)
    )

    margin = margin or 0
    spacing = spacing or 0

    -- setup self
    self.image = love.graphics.newImage(image)

    self.columns = columns
    self.rows = rows

    self.tile_width = (self.image:getWidth() - margin * 2 - spacing * (columns - 1)) / columns
    self.tile_height = (self.image:getHeight() - margin * 2 - spacing * (rows - 1)) / rows

    self.margin = margin
    self.spacing = spacing

    self.tiles = {}

    -- setup quads
    for y = 1, self.rows do
        for x = 1, self.columns do
            local tile = {}
            tile.quad = love.graphics.newQuad(
                (x - 1) * (self.tile_width + self.spacing) + self.margin,
                (y - 1) * (self.tile_height + self.spacing) + self.margin,
                self.tile_width,
                self.tile_height,
                self.image:getWidth(),
                self.image:getHeight()
            )
            tile.anim = nil

            table.insert(self.tiles, tile)
        end
    end

    -- setup animations
    if animations then
        for i = 1, #animations do
            self.tiles[v.id + 1].anim = animations[i]
        end
    end
end

function Tileset:getTile(i)
    return self.tiles[i + 1]
end

function Tileset:getTileQuad(i)
    return self.tiles[i + 1].quad
end

function Tileset:getTileAnimation(i)
    return self.tiles[i + 1].anim
end

function Tileset:release()
    self.image:release()

    for i = 1, #self.tiles do
        self.tiles[i].quad:release()
    end
end

return Tileset

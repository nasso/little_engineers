--- Argument checking utilities.
-- @module args

local args = {}

local CHECKS_ENABLED = true

local function is_of_type(v, t)
    local tt = type(t)

    return
        (tt == "string")
            and (type(v) == t)
end

--- Disable argument checking.
-- When disabled, functions such as `args.needs`,
-- `args.opt` and `args.any` will return immediately,
-- bypassing any argument checking. It is recommended
-- to let checks enabled during development.
-- By default, argument checking is enabled.
-- @return nil
function args.disable_checks()
    CHECKS_ENABLED = false
end

--- Enable argument checking.
-- By default, argument checking is enabled.
-- @return nil
function args.enable_checks()
    CHECKS_ENABLED = true
end

--- Assert that `v` is of type `t`.
--
-- This function does nothing if argument checking is disabled.
-- @class function
-- @name needs
-- @param v the value to check
-- @tparam string|{string,...} t the type(s) of the value
-- @string n the name of the value
-- @return nil
-- @see args.opt
function args.needs(v, t, n)
    if not CHECKS_ENABLED then return end

    local tv = type(v)
    local tt = type(t)
    local tn = type(n)

    assert((n == nil) or (tn == "string"), "the name of the value must be a string; got: " .. tn)

    local emsg = "invalid argument"

    if tt == "string" then
        if tn == "string" then
            emsg = "expected '" .. n .. "' to be a " .. t .. "; got: "
        else
            emsg = "expected " .. t .. "; got: "
        end

        assert(tv == t, emsg .. tv)
    elseif tt == "table" then
        if tn == "string" then
            emsg = "expected '" .. n .. "' to be one of: "
        else
            emsg = "expected one of: "
        end

        local found = false
        local count = #t

        for i, ti in pairs(t) do
            if is_of_type(v, ti) then
                found = true
                break
            else
                emsg = emsg .. ti

                if i < count then emsg = emsg .. ", " end
            end
        end

        assert(found, emsg .. "; got: " .. tv .. " (" .. tostring(v) .. ")")
    else
        if tn == "string" then
            emsg = "invalid argument: '" .. n .. "'"
        end

        assert(v ~= nil, emsg)
    end
end

--- Assert that `v` is either `nil`, or of type `t`.
--
-- This function does nothing if argument checking is disabled.
-- @param v the value to check
-- @tparam ?tab|string t the type of the value
-- @string n the name of the value
-- @return nil
-- @see args.needs
function args.opt(v, t, n)
    if not CHECKS_ENABLED then return end

    if v ~= nil then args.needs(v, t, n) end
end

--- Assert that at least one of the arguments isn't `nil`.
--
-- This function does nothing if argument checking is disabled.
-- @tparam {{string,value},...} alist argument list
-- @return nil
function args.any(...)
    if not CHECKS_ENABLED then return end

    local alist = { ... }

    local acount = 0

    for _, v in pairs(alist) do
        if v[2] ~= nil then
            return
        end

        acount = acount + 1
    end

    local emsg = "at least one of the following arguments mustn't be nil: "

    local i = 0
    for _, v in pairs(alist) do
        emsg = emsg .. v[1]
        i = i + 1

        if i + 1 == acount then
            emsg = emsg .. " and "
        elseif i + 1 < acount then
            emsg = emsg .. ", "
        end
    end

    error(emsg)
end

--- Return `v` if `v` isn't `nil`, `...` otherwise.
-- @param v the value of `v`
-- @param ... the default value
-- @return `v` or `...`
function args.get_or(v, ...)
    if v ~= nil then return v else return ... end
end

return args

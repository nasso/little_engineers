local Class = require "hump.class"

local Entity = require "utils.ecs.entity"

local Component = require "utils.ecs.component"

local Anchor = require "game.components.anchor"
local Button = require "game.components.button"
local Rectangle = require "game.components.rectangle"
local Transform = require "game.components.transform"
local MouseInputComponent = require "game.components.mouseinput"
local Sprite = require "game.components.sprite"

local gutils = {}

-- Util class to create a simple UI panel
gutils.Panel = Class {}
gutils.Panel:include(Entity)

gutils.Panel.EVENT_RECT_CHANGED = "panel_rect_changed"

function gutils.Panel:init(options)
    assert(type(options) == "table", "options must be a table")

    Entity.init(self, options.name)
    self._panel = true

    self._parent = nil

    self._parent_event_listener = function(e, ...)
        if e == gutils.Panel.EVENT_RECT_CHANGED then
            self:refresh_components_bounds_parent(...)
        end
    end

    self:add_event_listener(function(e, ...)
        if e == gutils.Panel.EVENT_RECT_CHANGED then
            self:refresh_components_bounds(...)
        end
    end)

    -- All panels have those
    self:add(Transform(), Anchor())

    self:set_rect(
        options.x or 0,
        options.y or 0,
        options.width or 1,
        options.height or 1
    )

    self:set_parent(options.parent)

    if options.anchor then
        self:set_anchor(unpack(options.anchor))
    end

    if options.sprite then
        self:set_sprite(options.sprite[1], options.sprite[2])

        if options.sprite_xform then
            self:set_sprite_xform(unpack(options.sprite_xform))
        end
    end

    self:set_fill(options.fill)
    self:set_stroke(options.stroke)

    if options.stroke_width then
        self:set_stroke_width(options.stroke_width)
    end
end

-- methods
function gutils.Panel:refresh_components_bounds(x, y, w, h)
    local xform = self:get(Transform)
    local rect = self:get(Rectangle)

    if xform then
        xform.x = x
        xform.y = y
    end

    if rect then
        rect.w = w
        rect.h = h
    end
end

function gutils.Panel:refresh_components_bounds_parent(x, y, w, h)
    self:get(Anchor).rect = {
        0,
        0,
        w,
        h
    }
end

function gutils.Panel:ensure_has_comp(comp)
    if not self:has(comp) then
        self:add(comp())
    end

    self:refresh_components_bounds(self._x, self._y, self._width, self._height)

    return self:get(comp)
end

-- setters
function gutils.Panel:set_parent(e)
    if self._parent ~= nil then
        self._parent:remove_event_listener(self._parent_event_listener)
    end

    local xform = self:get(Transform)

    xform:set_parent(e)
    self._parent = e

    -- update the anchor rectangle base
    if e and e._panel then
        self._parent:add_event_listener(self._parent_event_listener)

        self:refresh_components_bounds_parent(e._x, e._y, e._width, e._height)
    end
end

function gutils.Panel:set_pos(x, y)
    self:set_rect(x, y, self._width, self._height)
end

function gutils.Panel:set_rect(x, y, w, h)
    self._x = x
    self._y = y
    self._width = w
    self._height = h

    self:trigger_event(gutils.Panel.EVENT_RECT_CHANGED, x, y, w, h)
end

function gutils.Panel:set_anchor(horizontal, vertical)
    local anchor_comp = self:get(Anchor)

    anchor_comp.horizontal = horizontal
    anchor_comp.vertical = vertical
end

function gutils.Panel:set_fill(fill)
    local rect = self:ensure_has_comp(Rectangle)

    rect.fill = fill
end

function gutils.Panel:set_stroke(stroke)
    local rect = self:ensure_has_comp(Rectangle)

    rect.stroke = stroke
end

function gutils.Panel:set_stroke_width(stroke_width)
    assert(
        type(stroke_width) == "number",
        "stroke_width must be a number, got " .. type(stroke_width)
    )

    local rect = self:ensure_has_comp(Rectangle)

    rect.stroke_width = stroke_width
end

function gutils.Panel:set_sprite(tileset, tileid)
    local sprt = self:ensure_has_comp(Sprite)

    sprt.tileset = tileset
    sprt.tileid = tileid
end

function gutils.Panel:set_sprite_xform(x, y, r, sx, sy, ox, oy, kx, ky)
    local sprt = self:ensure_has_comp(Sprite)

    if x then sprt.x = x end
    if y then sprt.y = y end
    if r then sprt.r = r end
    if sx then sprt.sx = sx end
    if sy then sprt.sy = sy end
    if ox then sprt.ox = ox end
    if oy then sprt.oy = oy end
    if kx then sprt.kx = kx end
    if ky then sprt.ky = ky end
end

-- getters
function gutils.Panel:get_parent()
    return self._parent
end

function gutils.Panel:get_pos(x, y)
    return self._x, self._y
end

function gutils.Panel:get_rect()
    return self._x, self._y, self._width, self._height
end

function gutils.Panel:get_anchor()
    local anchor = self:get(Anchor)

    if anchor then
        return anchor.horizontal, anchor.vertical
    else
        return nil
    end
end

function gutils.Panel:get_fill()
    local rect = self:get(Rectangle)

    if rect then
        return rect.fill
    else
        return nil
    end
end

function gutils.Panel:get_stroke()
    local rect = self:get(Rectangle)

    if rect then
        return rect.stroke
    else
        return nil
    end
end

function gutils.Panel:get_stroke_width()
    local rect = self:get(Rectangle)

    if rect then
        return rect.stroke_width
    else
        return nil
    end
end

function gutils.Panel:get_sprite()
    local sprt = self:get(Sprite)

    if sprt then
        return sprt.tileset, sprt.tileid
    else
        return nil
    end
end

function gutils.Panel:get_sprite_xform()
    local sprt = self:get(Sprite)

    if sprt then
        return sprt.x, sprt.y, sprt.r, sprt.sx, sprt.sy, sprt.ox, sprt.oy, sprt.kx, sprt.ky
    else
        return nil
    end
end

-- Util class to create a simple UI button entity
gutils.Button = Class {}
gutils.Button:include(gutils.Panel)

function gutils.Button:init(options)
    gutils.Panel.init(self, options)

    assert(type(options.action) == "function", "no action function given")

    -- every button needs those
    self:add(
        MouseInputComponent(),
        Button(0, 0, self._width, self._height)
    )

    self:set_action(options.action)
end

function gutils.Button:refresh_components_bounds(x, y, w, h)
    gutils.Panel.refresh_components_bounds(self, x, y, w, h)

    local btn = self:get(Button)

    if btn then
        btn.w = w
        btn.h = h
    end
end

function gutils.Button:set_action(action)
    self:get(Button).action = function(...) action(self, ...) end
end

return gutils

local gutils = require "game.utils"

local Entity = require "utils.ecs.entity"

local Component = require "utils.ecs.component"

local Anchor = require "game.components.anchor"
local Button = require "game.components.button"
local Rectangle = require "game.components.rectangle"
local MouseInputComponent = require "game.components.mouseinput"

local Blocks = require "assets.blocks"

-- Constants
local QUICK_ACCESS_BLOCKS_COUNT = 5
local INVENTORY_CELL_SIZE = 70

-- Build UI
local function UI(controls_tileset, blocks_tileset, son_item, scale)
    scale = scale or 1

    local entities = {}

    -- button to start and stop the simulation
    local play_btn = gutils.Button({
        anchor = { "left", "top" };

        x = scale * 8;
        y = scale * 8;
        width = scale * 80;
        height = scale * 80;

        sprite = { controls_tileset, 2 };
        sprite_xform = {
            scale * 40,
            scale * 40,
            0,
            scale * 80,
            scale * 80
        };

        fill = { 0.5, 0.95, 0.6 };

        action = function(self, w, btn)
            if btn == 1 then
                local state = w.resources.state

                if state:is_simulation_running() then
                    state:stop_game()
                    self:set_sprite(controls_tileset, 2)
                else
                    state:launch_game()
                    self:set_sprite(controls_tileset, 1)
                end
            end
        end;
    })

    table.insert(entities, play_btn)

    local reset_btn = gutils.Button({
        anchor = { "left", "top" };

        x = scale * 8;
        y = scale * 96;
        width = scale * 80;
        height = scale * 80;

        sprite = { controls_tileset, 3 };
        sprite_xform = {
            scale * 40,
            scale * 40,
            0,
            scale * 80 * 0.8,
            scale * 80 * 0.8
        };

        fill = { 11/255, 51/255, 92/255 };
        stroke = { 0, 0, 0 };
        stroke_width = 2 / (scale * 80);

        action = function(self, w, btn)
            if btn == 1 then
                local state = w.resources.state

                state:reset_grid()
            end
        end;
    })

    table.insert(entities, reset_btn)

    -- quick access inventory panel (on the right)
    local quick_inv = gutils.Panel({
        anchor = { "right", "middle" };

        x = scale * (-INVENTORY_CELL_SIZE - 16);
        y = scale * (-QUICK_ACCESS_BLOCKS_COUNT * (INVENTORY_CELL_SIZE + 16) * 0.5);
        width = scale * (INVENTORY_CELL_SIZE + 16);
        height = scale * ((QUICK_ACCESS_BLOCKS_COUNT + 0.5) * (INVENTORY_CELL_SIZE + 8));

        fill = { 0.3, 0.3, 0.3 };
    })

    table.insert(entities, quick_inv)

    local active_btn = nil

    -- quick access inventory items
    for i = 1, QUICK_ACCESS_BLOCKS_COUNT do
        local item_id = Blocks[i] or Blocks[0]

        local item = gutils.Button({
            parent = quick_inv;

            x = scale * (8);
            y = scale * (8 + (INVENTORY_CELL_SIZE + 16) * (i - 1));
            width = scale * (INVENTORY_CELL_SIZE);
            height = scale * (INVENTORY_CELL_SIZE);

            sprite = { blocks_tileset, item_id.sprite_id };
            sprite_xform = {
                INVENTORY_CELL_SIZE / 2,
                INVENTORY_CELL_SIZE / 2,
                0,
                INVENTORY_CELL_SIZE,
                INVENTORY_CELL_SIZE
            };

            stroke_width = scale * 16;

            action = function(self, w, btn)
                love.audio.play(son_item)

                if btn == 1 then
                    -- change the active button
                    active_btn:set_fill(nil)
                    active_btn:set_stroke(nil)
                    self:set_fill({ 0.4, 0.4, 0.4 })
                    self:set_stroke({ 0.4, 0.4, 0.4 })
                    active_btn = self

                    w.resources.inventory.current_item = item_id
                end
            end;
        })

        if i == 1 then
            active_btn = item
            item:set_stroke({ 0.4, 0.4, 0.4 })
            item:set_fill({ 0.4, 0.4, 0.4 })
        end

        table.insert(entities, item)
    end

    return entities
end

return UI

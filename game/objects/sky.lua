local Entity = require "utils.ecs.entity"

local SkyComponent = require "game.components.sky"

local function Sky()
    local self = Entity("sky")

    self:add(SkyComponent({
        color = { 0.529, 0.808, 0.980 };
    }))

    return self
end

return Sky

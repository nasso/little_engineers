local Entity = require "utils.ecs.entity"

local BuildGridComponent = require "game.components.buildgrid"
local Transform = require "game.components.transform"
local MouseInputComponent = require "game.components.mouseinput"

local function BuildGrid(tileset, cols, rows, x, y)
    local self = Entity("buildgrid")

    cols = cols or 10
    rows = rows or 6
    x = x or 0
    y = y or 0

    self:add(BuildGridComponent(tileset, cols, rows))
    self:add(Transform(x, y - rows))
    self:add(MouseInputComponent())

    return self
end

return BuildGrid

local Entity = require "utils.ecs.entity"

local PhysicsBody = require "game.components.physicsbody"
local Terrain = require "game.components.terrain"

function Ground()
    local self = Entity("ground")

    self:add(
        Terrain()
            :addLayer({ 101 / 255, 204 / 255, 73 / 255 }, 0.0) -- grass
            :addLayer({ 142 / 255, 103 / 255, 11 / 255 }, -0.1) -- dirt
            :addLayer({ 145 / 255, 145 / 255, 145 / 255 }, -1.0) -- stone
    )

    self:add(
        PhysicsBody("static")
            :fix(PhysicsBody.EdgeShape(-1000, 0, 1000, 0))
    )

    return self
end

return Ground

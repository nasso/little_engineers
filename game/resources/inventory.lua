local utils = require "utils"

local Class = require "hump.class"
local Blocks = require "assets.blocks"

local InventoryResource = Class {}

function InventoryResource:init()
    self.current_item = Blocks[1]
    self.rotation = 0
end

function InventoryResource:rotate()
    self.rotation = (self.rotation + 1) % 4
end

function InventoryResource:create_item()
    local item = utils.copy(self.current_item, true)

    item.rotation = self.rotation

    local cos = math.cos(item.rotation * 0.5 * math.pi)
    local sin = math.sin(item.rotation * 0.5 * math.pi)

    if item.fix then
        for i = 1, item.rotation do
            local f = item.fix[4]
            item.fix[4] = item.fix[3]
            item.fix[3] = item.fix[2]
            item.fix[2] = item.fix[1]
            item.fix[1] = f
        end
    end

    if item.shapes then
        for i=1, #item.shapes do
            local shape = item.shapes[i]

            if shape.type == "circle" then
                shape.x, shape.y =
                    (shape.x - 0.5) * cos - (shape.y - 0.5) * sin + 0.5,
                    (shape.x - 0.5) * sin + (shape.y - 0.5) * cos + 0.5
            elseif shape.type == "rectangle" then
                local cx, cy = shape.x + shape.w * 0.5, shape.y + shape.h * 0.5

                cx, cy =
                    (cx - 0.5) * cos - (cy - 0.5) * sin + 0.5,
                    (cx - 0.5) * sin + (cy - 0.5) * cos + 0.5

                if item.rotation % 2 ~= 0 then
                    shape.h, shape.w = shape.w, shape.h
                end

                shape.x, shape.y = cx - shape.w * 0.5, cy - shape.h * 0.5
            end
        end
    end

    if item.actions then
        for i=1, #item.actions do
            local a = item.actions[i]

            a.x = a.x or 0
            a.y = a.y or 0
            a.dx = a.dx or 0
            a.dy = a.dy or 0

            a.x, a.y =
                (a.x - 0.5) * cos - (a.y - 0.5) * sin + 0.5,
                (a.x - 0.5) * sin + (a.y - 0.5) * cos + 0.5

            a.dx, a.dy =
                (a.dx - 0.5) * cos - (a.dy - 0.5) * sin + 0.5,
                (a.dx - 0.5) * sin + (a.dy - 0.5) * cos + 0.5
        end
    end

    return item
end

return InventoryResource

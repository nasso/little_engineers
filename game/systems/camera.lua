local Camera = require "game.components.camera"
local Settings = require "assets.settings"
local System = require "utils.ecs.system"

-- Camera system
local CameraSystem = System.new()
CameraSystem.filter = System.require_all(Camera)

function CameraSystem:on_draw(w)
    for _, e in ipairs(self.entities) do
        local cam = e:get(Camera)

        love.graphics.push()

        love.graphics.scale(cam.zoom)
        love.graphics.translate(-cam.x, -cam.y)
        love.graphics.rotate(cam.rot)
    end
end

return CameraSystem

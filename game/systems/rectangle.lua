local Rectangle = require "game.components.rectangle"
local System = require "utils.ecs.system"
local TransformSystem = require "game.systems.sub.transform"

-- Sky system
local RectangleSystem = System.new()
RectangleSystem.filter = System.require_all(Rectangle)

function RectangleSystem:on_draw(w)
    for _, e in ipairs(self.entities) do
        TransformSystem.pre(e)

        local rect = e:get(Rectangle)

        if rect.fill then
            love.graphics.setColor(
                rect.fill[1],
                rect.fill[2],
                rect.fill[3],
                rect.fill[4] or 1
            )

            love.graphics.rectangle(
                "fill",
                rect.x,
                rect.y,
                rect.w,
                rect.h
            )
        end

        if rect.stroke then
            love.graphics.setLineWidth(rect.stroke_width)

            love.graphics.setColor(
                rect.stroke[1],
                rect.stroke[2],
                rect.stroke[3],
                rect.stroke[4] or 1
            )

            love.graphics.rectangle(
                "line",
                rect.x,
                rect.y,
                rect.w,
                rect.h
            )
        end

        TransformSystem.post(e)
    end
end

return RectangleSystem

local Settings = require "assets.settings"

local utils = require "utils"
local System = require "utils.ecs.system"

local Sprite = require "game.components.sprite"
local TransformSystem = require "game.systems.sub.transform"

-- Srite rendering system
local SpriteRenderingSystem = System.new()
SpriteRenderingSystem.filter = System.require_all(Sprite)

function SpriteRenderingSystem:on_update(w, dt)
    for _, e in ipairs(self.entities) do
        local sprite = e:get(Sprite)

        if sprite.tileset and sprite.animated then
            local anim = sprite.tileset:getTileAnimation(sprite.tileid)

            if anim ~= nil then
                sprite.t = sprite.t + dt * 1000

                if anim[sprite.frame].duration <= sprite.t then
                    sprite.frame = sprite.frame + 1
                    sprite.t = 0

                    if sprite.frame > #anim then
                        sprite.frame = 1
                    end
                end
            end
        else
            sprite:resetAnimation()
        end
    end
end

function SpriteRenderingSystem:on_draw(w)
    love.graphics.setColor(1, 1, 1, 1)

    for _, e in ipairs(self.entities) do
        local sprite = e:get(Sprite)

        if sprite.tileset then
            TransformSystem.pre(e)

            local quad = sprite.tileset:getTileQuad(sprite:getCurrentTile())
            local quadw, quadh = sprite.tileset.tile_width, sprite.tileset.tile_height

            love.graphics.draw(
                sprite.tileset.image,
                quad,
                sprite.x,
                sprite.y,
                sprite.r,
                sprite.sx / quadw,
                sprite.sy / quadh,
                sprite.ox + 0.5 * quadw,
                sprite.oy + 0.5 * quadh,
                sprite.kx,
                sprite.ky
            )

            TransformSystem.post(e)
        end
    end
end

return SpriteRenderingSystem

local utils = require "utils"

local System = require "utils.ecs.system"

local Terrain = require "game.components.terrain"
local Camera = require "game.components.camera"

local TerrainSystem = System.new()
TerrainSystem.filter = System.require_any(Terrain, Camera)

function TerrainSystem:on_draw(w)
    local bounds_x1, bounds_y1, bounds_x2, bounds_y2 = 0, 0, love.graphics.getDimensions()

    for _, ecam in ipairs(self.entities) do
        local cam = ecam:get(Camera)

        if cam ~= nil then
            bounds_x1, bounds_y1 = cam:world_coords(bounds_x1, bounds_y1)
            bounds_x2, bounds_y2 = cam:world_coords(bounds_x2, bounds_y2)
        end
    end

    for _, eter in ipairs(self.entities) do
        local ter = eter:get(Terrain)

        if ter ~= nil then
            for _, layer in ipairs(ter.layers) do
                local h = bounds_y2 - layer.alt

                if h > 0 then
                    love.graphics.setColor(
                        layer.color[1],
                        layer.color[2],
                        layer.color[3],
                        layer.color[4] or 1
                    )

                    love.graphics.rectangle(
                        "fill",
                        bounds_x1,
                        -layer.alt,
                        bounds_x2 - bounds_x1,
                        h
                    )
                end
            end
        end
    end
end

return TerrainSystem

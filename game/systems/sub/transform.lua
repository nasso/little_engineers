local Settings = require "assets.settings"

local Anchor = require "game.components.anchor"
local Transform = require "game.components.transform"

-- Transform system
local TransformSystem = {}

local function apply_xform(anchor, xform)
    if xform ~= nil and xform.parent ~= nil then
        apply_xform(xform.parent:get(Anchor), xform.parent:get(Transform))
    end

    -- Anchors
    if anchor ~= nil then
        local anchor_hor = anchor.horizontal
        local anchor_ver = anchor.vertical
        local anchor_rect = anchor.rect

        local anchor_x, anchor_y, rect_x, rect_y, rect_w, rect_h

        if anchor_rect then
            rect_x = anchor_rect[1]
            rect_y = anchor_rect[2]
            rect_w = anchor_rect[3]
            rect_h = anchor_rect[4]
        else
            rect_x = 0
            rect_y = 0
            rect_w, rect_h = love.graphics.getDimensions()
        end

        if anchor_hor == "left" then
            anchor_x = rect_x
        elseif anchor_hor == "center" then
            anchor_x = rect_x + rect_w * 0.5
        elseif anchor_hor == "right" then
            anchor_x = rect_x + rect_w
        end

        if anchor_ver == "top" then
            anchor_y = rect_y
        elseif anchor_ver == "middle" then
            anchor_y = rect_y + rect_h * 0.5
        elseif anchor_ver == "bottom" then
            anchor_y = rect_y + rect_h
        end

        if anchor_x ~= nil and anchor_y ~= nil then
            love.graphics.translate(anchor_x, anchor_y)
        end
    end

    if xform ~= nil then
        love.graphics.translate(xform.x, xform.y)
        love.graphics.rotate(xform.r)
        love.graphics.scale(xform.sx, xform.sy)
        love.graphics.shear(xform.kx, xform.ky)
        love.graphics.translate(-xform.ox, -xform.oy)
    end
end

function TransformSystem.pre(e)
    local anchor = e:get(Anchor)
    local xform = e:get(Transform)

    if anchor ~= nil or xform ~= nil then
        love.graphics.push()

        apply_xform(anchor, xform)
    end
end

function TransformSystem.post(e)
    local anchor = e:get(Anchor)
    local xform = e:get(Transform)

    if anchor ~= nil or xform ~= nil then
        if Settings.debug["transform"] then
            local r, g, b, a = love.graphics.getColor()

            love.graphics.setLineWidth(0.05)

            love.graphics.setColor(1, 0, 0, 1)
            love.graphics.line(0, 0, 10, 0)

            love.graphics.setColor(0, 1, 0, 1)
            love.graphics.line(0, 0, 0, 10)

            love.graphics.setColor(1, 1, 1, 1)
            love.graphics.circle("fill", 0, 0, 0.1)

            love.graphics.setColor(r, g, b, a)
        end

        love.graphics.pop()
    end
end

return TransformSystem

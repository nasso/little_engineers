local System = require "utils.ecs.system"
local utils = require "utils"

local BuildGrid = require "game.components.buildgrid"
local MouseInputComponent = require "game.components.mouseinput"

local TransformSystem = require "game.systems.sub.transform"

-- Build grid system
local BuildGridSystem = System.new()
BuildGridSystem.filter = System.require_all(BuildGrid)

function BuildGridSystem:on_draw(w)
    for _, e in ipairs(self.entities) do
        TransformSystem.pre(e)

        local grid = e:get(BuildGrid)
        local mouse = e:get(MouseInputComponent)

        -- Place elements
        local mx, my = love.graphics.inverseTransformPoint(mouse.x, mouse.y)
        local mcol = math.floor(mx)
        local mrow = math.floor(my)

        if mcol >= 0 and mrow >= 0 and mcol < grid.columns and mrow < grid.rows then
            local inventory = w.resources["inventory"]

            if mouse.pressed[1] then
                local son = w.resources.sons[inventory.current_item.sound]

                if son then
                    love.audio.stop(son)
                    love.audio.play(son)
                end

                grid.data[mcol + 1][mrow + 1] = inventory:create_item()
            elseif mouse.pressed[2] then
                local son = w.resources.son_pop
                love.audio.stop(son)
                love.audio.play(son)

                grid.data[mcol + 1][mrow + 1] = nil
                love.audio.play(son)
            end
        end

        -- Draw everything
        local margins = grid.margins
        local half_margins = margins / 2

        love.graphics.setColor(0, 0, 0, 0.2)
        for column = 0, grid.columns - 1 do
            for row = 0, grid.rows - 1 do
                if row == mrow and column == mcol then
                    love.graphics.setColor(0, 0, 0, 0.3)
                else
                    love.graphics.setColor(0, 0, 0, 0.2)
                end

                -- background rect
                love.graphics.rectangle(
                    "fill",
                    column + half_margins,
                    row + half_margins,
                    1 - margins,
                    1 - margins,
                    margins, margins
                )

                -- (eventual) item
                local item = grid.data[column + 1][row + 1]

                if item ~= nil then
                    local quad = grid.tileset:getTileQuad(item.sprite_id)
                    local quadw, quadh = grid.tileset.tile_width, grid.tileset.tile_height

                    love.graphics.setColor(1, 1, 1, 1)
                    love.graphics.draw(
                        grid.tileset.image,
                        quad,
                        column + 0.5,
                        row + 0.5,
                        (item.rotation or 0) * 0.5 * math.pi,
                        1 / quadw,
                        1 / quadh,
                        0.5 * quadw,
                        0.5 * quadh
                    )
                end
            end
        end

        TransformSystem.post(e)
    end
end

return BuildGridSystem

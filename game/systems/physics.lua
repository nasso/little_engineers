local Settings = require "assets.settings"

local Class = require "hump.class"

local System = require "utils.ecs.system"
local PhysicsBody = require "game.components.physicsbody"
local Transform = require "game.components.transform"

local TransformSystem = require "game.systems.sub.transform"

--- Check for shapes
local function check_shape(self, shape)
    local pshape = self.shapes[shape]

    if pshape == nil then
        -- Creation...
        if shape.type == "chain" then
            pshape = love.physics.newChainShape(shape.loop, shape.points)
        elseif shape.type == "circle" then
            pshape = love.physics.newCircleShape(shape.x, shape.y, shape.radius)
        elseif shape.type == "edge" then
            pshape = love.physics.newEdgeShape(shape.x1, shape.y1, shape.x2, shape.y2)
        elseif shape.type == "polygon" then
            pshape = love.physics.newPolygonShape(shape.points)
        elseif shape.type == "rectangle" then
            pshape = love.physics.newRectangleShape(shape.x, shape.y, shape.width, shape.height, shape.angle)
        else
            error("unknown shape type: " .. shape.type)
        end

        shape.obj = pshape

        self.shapes[shape] = pshape
    end

    return pshape
end

--- Check for fixtures
local function check_fixture(self, pbody, fix)
    local pfix = pbody.fixtures[fix]

    if pfix == nil then
        -- Create it if it doesn't exist
        pfix = love.physics.newFixture(pbody.body, check_shape(self, fix.shape), fix.density)
        pfix:setFriction(fix.friction)

        fix.obj = pfix
        pbody.fixtures[fix] = pfix
    end

    return pfix
end

--- Check for bodies
local function check_body(self, body)
    local pbody = self.bodies[body]

    if pbody == nil then
        -- Create it if it doesn't exist
        pbody = {}
        pbody.body = love.physics.newBody(self.physicsworld, 0, 0, body.type)
        pbody.fixtures = {}
        pbody.last_use = 0

        body.obj = pbody.body

        self.bodies[body] = pbody
    end

    -- Check fixtures
    for _, v in ipairs(body.fixtures) do
        check_fixture(self, pbody, v)
    end

    for i = 1, #body.forces do
        local f = body.forces[i]

        local a = pbody.body:getAngle()
        local x, y = pbody.body:getWorldPoint(f.x, f.y)

        local cos = math.cos(a)
        local sin = math.sin(a)

        pbody.body:applyForce(
            (f.dx or 0) * cos - (f.dy or 0) * sin,
            (f.dx or 0) * sin + (f.dy or 0) * cos,
            x,
            y
        )
    end

    for i = 1, #body.impulses do
        local p = body.impulses[i]

        local a = pbody.body:getAngle()
        local x, y = pbody.body:getWorldPoint(p.x, p.y)

        local cos = math.cos(a)
        local sin = math.sin(a)

        pbody.body:applyLinearImpulse(
            (p.dx or 0) * cos - (p.dy or 0) * sin,
            (p.dx or 0) * sin + (p.dy or 0) * cos,
            x,
            y
        )
    end

    body.forces = {}
    body.impulses = {}

    return pbody
end

-- Destroys an internal shape object
local function delete_pshape(pshape)
    pshape:destroy()
    pshape:release()
end

-- Destroys an internal body object
local function delete_pbody(pbody)
    for _, v in pairs(pbody.fixtures) do
        v:destroy()
        v:release()
    end

    pbody.fixtures = {}

    pbody.body:destroy()
    pbody.body:release()
end

-- Destroys the internal shape object associated with this shape
local function delete_shape(self, shape)
    local pshape = self.shapes[shape]

    if pshape then
        delete_pshape(pshape)
    end

    self.shapes[shape] = nil
end

-- Destroys the internal body object associated with this body
local function delete_body(self, body)
    local pbody = self.bodies[body]

    if pbody ~= nil then
        delete_pbody(pbody)
    end

    self.bodies[body] = nil
end

-- GC bodies that haven't been used since at least the specified tick count
local function bodies_garbage_collect(self, last_use_max)
    for body, pbody in pairs(self.bodies) do
        if self.tick_clock - pbody.last_use >= last_use_max then
            delete_body(self, body)
        end
    end
end

local PhysicsSystem = System.new(Class {})
PhysicsSystem.filter = System.require_all(PhysicsBody)

function PhysicsSystem:init(...)
    self.physicsworld = love.physics.newWorld(...)

    -- checked buffers (caches)
    self.bodies = {}
    self.shapes = {}
    self.tick_clock = 0
end

function PhysicsSystem:on_remove_from_world()
    for _, v in pairs(self.bodies) do
        delete_pbody(self, v)
    end

    self.bodies = nil

    for _, v in pairs(self.shapes) do
        delete_pshape(self, v)
    end

    self.shapes = nil

    self.physicsworld:release()
    self.physicsworld = nil
end

function PhysicsSystem:on_update(w, dt)
    self.tick_clock = self.tick_clock + 1

    for i = 1, #self.entities do
        local e = self.entities[i]

        local body = e:get(PhysicsBody)
        local xform = e:get(Transform)

        -- check body
        local pbody = check_body(self, body)
        pbody.last_use = self.tick_clock

        if xform ~= nil then
            pbody.body:setPosition(xform.x, xform.y)
            pbody.body:setAngle(xform.r)
        else
            pbody.body:setPosition(0, 0)
            pbody.body:setAngle(0)
        end
    end

    -- delete bodies that haven't been used in the last frame
    bodies_garbage_collect(self, 1)

    self.physicsworld:update(dt)

    for i = 1, #self.entities do
        local e = self.entities[i]

        local body = e:get(PhysicsBody)
        local xform = e:get(Transform)

        -- update transform
        local pbody = check_body(self, body)

        if xform ~= nil then
            xform.x, xform.y = pbody.body:getPosition()
            xform.r = pbody.body:getAngle()
        end
    end
end

return PhysicsSystem

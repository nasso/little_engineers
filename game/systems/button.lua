local System = require "utils.ecs.system"

local TransformSubSystem = require "game.systems.sub.transform"

local ButtonComponent = require "game.components.button"
local MouseInputComponent = require "game.components.mouseinput"

local ButtonSystem = System.new()
ButtonSystem.filter = System.require_all(MouseInputComponent, ButtonComponent)

function ButtonSystem:on_update(w)
    for _, e in ipairs(self.entities) do
        TransformSubSystem.pre(e)

        local mouse = e:get(MouseInputComponent)
        local btn = e:get(ButtonComponent)

        local x, y = love.graphics.inverseTransformPoint(mouse.x, mouse.y)

        if
            -- bounds check
            x >= btn.x and
            y >= btn.y and
            x <= btn.x + btn.w and
            y <= btn.y + btn.h
        then
            -- button has been clicked
            for i = 1, mouse.button_count do
                if mouse.released[i] then
                    btn.action(w, i)
                end
            end
        end

        TransformSubSystem.post(e)
    end
end

return ButtonSystem

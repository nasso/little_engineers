local MouseInputComponent = require "game.components.mouseinput"
local System = require "utils.ecs.system"

-- Sky system
local MouseInputSystem = System.new()

MouseInputSystem.filter = System.require_all(MouseInputComponent)

function MouseInputSystem:on_update(w, dt)
    for _, e in ipairs(self.entities) do
        local mouse = e:get(MouseInputComponent)

        mouse.old_x = mouse.x
        mouse.old_y = mouse.y
        mouse.x, mouse.y = love.mouse.getPosition()
        mouse.dx = mouse.x - mouse.old_x
        mouse.dy = mouse.y - mouse.old_y

        -- TODO: mouse wheel

        for i = 1, mouse.button_count do
            local down = love.mouse.isDown(i)

            if mouse.down[i] ~= down then
                mouse.pressed[i] = down
                mouse.released[i] = not down
            else
                mouse.pressed[i] = false
                mouse.released[i] = false
            end

            mouse.down[i] = down
        end
    end
end

return MouseInputSystem

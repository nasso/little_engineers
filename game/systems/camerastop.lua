local Camera = require "game.components.camera"
local Settings = require "assets.settings"
local System = require "utils.ecs.system"

-- Camera system
local CameraStopSystem = System.new()
CameraStopSystem.filter = System.require_all(Camera)

function CameraStopSystem:on_draw(w)
    for _, e in ipairs(self.entities) do
        love.graphics.pop()
    end
end

return CameraStopSystem

local Settings = require "assets.settings"

local Class = require "hump.class"

local System = require "utils.ecs.system"
local PhysicsBody = require "game.components.physicsbody"

local TransformSystem = require "game.systems.sub.transform"

function hsla_to_rgba(h, s, l, a)
    if s <= 0 then
        return l, l, l, a
    end

    h, s, l = h * 6, s, l
    local c = (1 - math.abs(2 * l - 1)) * s
    local x = (1 - math.abs(h % 2 - 1)) * c
    local m, r, g, b = (l - 0.5 * c), 0, 0, 0

    if     h < 1 then r, g, b = c, x, 0
    elseif h < 2 then r, g, b = x, c, 0
    elseif h < 3 then r, g, b = 0, c, x
    elseif h < 4 then r, g, b = 0, x, c
    elseif h < 5 then r, g, b = x, 0, c
    else              r, g, b = c, 0, x
    end

    return (r + m), (g + m), (b + m), a
end

-- PhysicsDebugSystem
local PhysicsDebugSystem = System.new(Class {})
PhysicsDebugSystem.filter = System.require_all(PhysicsBody)

function PhysicsDebugSystem:init(target)
    assert(target, "A target PhysicsSystem must be specified")
    self.target = target

    self.hues = {}
end

function PhysicsDebugSystem:on_draw(w)
    if Settings.debug["physics"] then
        love.graphics.setLineWidth(0.05)

        for i = 1, #self.entities do
            local e = self.entities[i]

            local body = e:get(PhysicsBody)

            -- check body
            local pbody = self.target.bodies[body]

            if pbody ~= nil then
                if not self.hues[pbody] then
                    self.hues[pbody] = math.random()
                end

                local hue = self.hues[pbody]

                TransformSystem.pre(e)

                for k, v in pairs(pbody.fixtures) do
                    local shape = v:getShape()
                    local shape_type = shape:getType()

                    love.graphics.setColor(hsla_to_rgba(hue, 1, 0.6 - v:getFriction() * 0.2))

                    if shape_type == "circle" then
                        local cx, cy = shape:getPoint()

                        love.graphics.circle(
                            "line",
                            cx,
                            cy,
                            shape:getRadius()
                        )
                    elseif shape_type == "polygon" then
                        love.graphics.polygon("line", shape:getPoints())
                    elseif shape_type == "edge" or shape_type == "chain" then
                        love.graphics.line(shape:getPoints())
                    end
                end

                TransformSystem.post(e)
            end
        end
    end
end

return PhysicsDebugSystem

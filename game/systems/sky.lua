local Sky = require "game.components.sky"
local System = require "utils.ecs.system"

-- Sky system
local SkySystem = System.new()
SkySystem.filter = System.require_all(Sky)

function SkySystem:on_draw(w, dt)
    for _, e in ipairs(self.entities) do
        local sky = e:get(Sky)

        love.graphics.clear(sky.color[1], sky.color[2], sky.color[3], sky.color[4] or 1)
    end
end

return SkySystem

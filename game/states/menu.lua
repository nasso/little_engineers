local Gamestate = require "hump.gamestate"
local Gamestate = require "hump.gamestate"

local gutils = require "game.utils"
local World = require "utils.ecs.world"
local Entity = require "utils.ecs.entity"
local Tileset = require "utils.tileset"

local Ground = require "game.objects.ground"
local Sky = require "game.objects.sky"

local CameraComponent = require "game.components.camera"
local Transform = require "game.components.transform"
local Sprite = require "game.components.sprite"

local BUTTON_SIZE = 125;

local MenuState = {}

function MenuState:init()
    self.controls_tileset = Tileset("assets/textures/controls.png", 6, 1, 0, 0)

    local camera_entity = Entity("camera")
    self.camera = CameraComponent(0.0, -10.0, 70.0, 0.0)
    camera_entity:add(self.camera)

    -- Setup world
    self.world = World()

    self.world:add_entities({
        Sky(),
        Ground(),
        camera_entity
    })

    -- menu ui
    self.ui_world = World()
    self.ui_world.resources = self.world.resources
    self.ui_world:add_entities({
        -- bouton "aide"
        gutils.Button({
            anchor = { "center", "middle" };

            x = -BUTTON_SIZE * 2;
            y = -BUTTON_SIZE * 0.5;
            width = BUTTON_SIZE;
            height = BUTTON_SIZE;

            sprite = { self.controls_tileset, 5 };
            sprite_xform = { BUTTON_SIZE * 0.5, BUTTON_SIZE * 0.5, 0, BUTTON_SIZE, BUTTON_SIZE };

            fill = { 252 / 255, 223 / 255, 0.0 };

            action = function()
                Gamestate.switch(require "game.states.tuto")
            end;
        }),

        -- bouton "play"
        gutils.Button({
            anchor = { "center", "middle" };

            x = -BUTTON_SIZE * 0.5;
            y = -BUTTON_SIZE * 0.5;
            width = BUTTON_SIZE;
            height = BUTTON_SIZE;

            sprite = { self.controls_tileset, 2 };
            sprite_xform = { BUTTON_SIZE * 0.5, BUTTON_SIZE * 0.5, 0, BUTTON_SIZE, BUTTON_SIZE };

            fill = { 252 / 255, 223 / 255, 0.0 };

            action = function()
                Gamestate.switch(require "game.states.gameplay")
            end;
        }),

        -- bouton "niveaux"
        gutils.Button({
            anchor = { "center", "middle" };

            x = BUTTON_SIZE;
            y = -BUTTON_SIZE * 0.5;
            width = BUTTON_SIZE;
            height = BUTTON_SIZE;

            sprite = { self.controls_tileset, 4 };
            sprite_xform = { BUTTON_SIZE * 0.5, BUTTON_SIZE * 0.5, 0, BUTTON_SIZE, BUTTON_SIZE };

            fill = { 252 / 255, 223 / 255, 0.0 };

            action = function() end;
        })
    })

    -- Add systems
    self.world:add_systems({
        require "game.systems.mouseinput" (),

        require "game.systems.camera" (),

        require "game.systems.sky" (),
        require "game.systems.terrain" (),
        require "game.systems.sprite" (),
        require "game.systems.rectangle" (),

        require "game.systems.camerastop" ()
    })

    self.ui_world:add_systems({
        require "game.systems.mouseinput" (),
        require "game.systems.button" (),
        require "game.systems.rectangle" (),
        require "game.systems.sprite" ()
    })

    love.graphics.reset()
end

function MenuState:update(dt)
    self.world:update(dt)
    self.ui_world:update(dt)
end

function MenuState:draw()
    self.world:draw()
    self.ui_world:draw()
end

return MenuState

local Settings = require "assets.settings"

local Blocks = require "assets.blocks"

local utils = require "utils"
local World = require "utils.ecs.world"
local Entity = require "utils.ecs.entity"
local Tileset = require "utils.tileset"

local InventoryResource = require "game.resources.inventory"

local UI = require "game.objects.gameplayui"
local BuildGrid = require "game.objects.buildgrid"
local Ground = require "game.objects.ground"
local Sky = require "game.objects.sky"

local CameraComponent = require "game.components.camera"
local BuildGridComponent = require "game.components.buildgrid"
local Transform = require "game.components.transform"
local Sprite = require "game.components.sprite"
local PhysicsBody = require "game.components.physicsbody"

-- Gameplay state
local GameplayState = {}

function GameplayState:init()
    self.blocks_tileset = Tileset(
        "assets/textures/tileset.png",

        -- columns, rows
        3, 2,

        -- margin, spacing
        0, 20
    )

    self.controls_tileset = Tileset("assets/textures/controls.png", 6, 1, 0, 0)

    self.sono = love.audio.newSource("assets/sons/gong.mp3", "static")
    self.item_son = love.audio.newSource("assets/sons/selek.mp3", "static")

    self.build_grid = BuildGrid(self.blocks_tileset, 12, 6, 0, 0)
    self.in_game = false
    self.machine_entities = nil
    self.machine_actions = nil

    local camera_entity = Entity("camera")
    self.camera = CameraComponent(-3.0, -10.0, 70.0, 0.0)
    camera_entity:add(self.camera)

    -- Setup world
    self.world = World()

    self.world.resources.inventory = InventoryResource()
    self.world.resources.state = self
    self.world.resources.son_pop = love.audio.newSource("assets/sons/popbulle.mp3", "static")
    self.world.resources.sons = {}

    for i, block in ipairs(Blocks) do
        if block.sound then
            self.world.resources.sons[block.sound] = love.audio.newSource(block.sound, "static")
        end
    end

    self.world:add_entities({
        Sky(),
        Ground(),
        camera_entity,
        self.build_grid
    })

    self.ui_world = World()
    self.ui_world.resources = self.world.resources
    self.ui_world:add_entities(UI(self.controls_tileset, self.blocks_tileset, self.item_son))

    -- Add systems
    local physics_system = require "game.systems.physics" (0, 9.81)
    self.world:add_systems({
        require "game.systems.mouseinput" (),
        physics_system,

        require "game.systems.camera" (),
        require "game.systems.sky" (),
        require "game.systems.terrain" (),
        require "game.systems.buildgrid" (),
        require "game.systems.sprite" (),
        require "game.systems.rectangle" (),

        -- debug systems
        require "game.systems.debug.physics" (physics_system),

        require "game.systems.camerastop" ()
    })

    self.ui_world:add_systems({
        require "game.systems.mouseinput" (),
        require "game.systems.button" (),
        require "game.systems.rectangle" (),
        require "game.systems.sprite" ()
    })

    love.graphics.reset()
end

function GameplayState:update(dt)
    local camera_speed = 0.1

    if love.keyboard.isDown("rctrl") then
        camera_speed = 0.2
    end

    if love.keyboard.isDown('up') then
        self.camera:move(0, -camera_speed * dt * 5000 / self.camera.zoom)
    end

    if love.keyboard.isDown('down') then
        self.camera:move(0, camera_speed * dt * 5000 / self.camera.zoom)
    end

    if love.keyboard.isDown('right') then
        self.camera:move(camera_speed * dt * 5000 / self.camera.zoom, 0)
    end

    if love.keyboard.isDown('left') then
        self.camera:move(-camera_speed * dt * 5000 / self.camera.zoom, 0)
    end

    if self.machine_actions then
        for k, v in ipairs({"a", "z", "e", "r", "t", "y"}) do
            local actions = self.machine_actions[k]

            if actions and love.keyboard.isDown(v) then
                for i = 1, #actions do
                    local a = actions[i]

                    a.x = actions.x
                    a.y = actions.y

                    if a.type == "force" then
                        local body = actions.entity:get(PhysicsBody)
                        table.insert(body.forces, a)
                    end
                end
            end
        end
    end

    self.world:update(dt)
    self.ui_world:update(dt)
end

function GameplayState:keypressed(key, sc, isrepeat)
    if key == "space" then
        self.world.resources.inventory:rotate()
    end

    if self.machine_actions then
        for k, v in ipairs({"a", "z", "e", "r", "t", "y"}) do
            local actions = self.machine_actions[k]

            if actions and key == v then
                for i = 1, #actions do
                    local a = actions[i]

                    a.x = actions.x
                    a.y = actions.y

                    if a.type == "impulse" then
                        local body = actions.entity:get(PhysicsBody)
                        table.insert(body.impulses, a)
                    end
                end
            end
        end
    end
end

function GameplayState:mousemoved(x, y, dx, dy, istouch)
    if love.mouse.isDown(3) then
        self.camera:move(-dx / self.camera.zoom, -dy / self.camera.zoom)
    end
end

function GameplayState:wheelmoved(dx, dy)
    if love.keyboard.isDown("lctrl") then
        if dy > 0 then
            f = 1.2
        else
            f = 0.8
        end

        -- local x, y = self.camera:world_coords(love.mouse.getPosition())

        -- local vx, vy = self.camera.x - x, self.camera.y - y

        -- self.camera.x = x + vx * f
        -- self.camera.y = y + vy * f

        self.camera.zoom = self.camera.zoom * f
    end
end

-- called each frame
function GameplayState:draw()
    self.world:draw()
    self.ui_world:draw()
end

function GameplayState:leave()
    self.blocks_tileset:release()

    self.world:clear_systems()
    self.world:clear_entities()
end

---------------
-- FUNCTIONS --
---------------

local function build_machine_part(ox, oy, part, action_list, tileset)
    local block_entities = {}

    local part_entity = Entity("machine_part")
    local part_body = PhysicsBody("dynamic")
    local part_xform = Transform(ox + part.x, oy + part.y)

    part_entity:add(part_xform)
    part_entity:add(part_body)

    -- add each block
    for i = 1, #part.data do
        local block = part.data[i]
        local value = block.value
        local x = block.x
        local y = block.y

        if value ~= nil then
            -- block entity (for the sprite)
            local be = Entity("machine_part_block")
            be:add(Transform(x, y):set_parent(part_entity))
            be:add(Sprite(tileset, value.sprite_id, 0.5, 0.5, value.rotation * 0.5 * math.pi))

            table.insert(block_entities, be)

            if value.actions then
                local actions_cp = utils.copy(value.actions, true)

                actions_cp.entity = part_entity
                actions_cp.x = (actions_cp.x or 0) + x + 0.5
                actions_cp.y = (actions_cp.y or 0) + y + 0.5

                table.insert(action_list, actions_cp)
            end

            -- add shape to part body
            if value.shapes ~= nil then
                for j = 1, #value.shapes do
                    local shape = value.shapes[j]
                    local pshape = nil

                    if shape.type == "rectangle" then
                        pshape = PhysicsBody.RectangleShape(
                            x + shape.x + shape.w * 0.5,
                            y + shape.y + shape.h * 0.5,
                            shape.w,
                            shape.h
                        )
                    elseif shape.type == "circle" then
                        pshape = PhysicsBody.CircleShape(shape.r, x + shape.x, y + shape.y)
                    end

                    if pshape then
                        part_body:fix(
                            pshape,
                            shape.density,
                            shape.friction
                        )
                    end
                end
            else
                part_body:fix(
                    PhysicsBody.RectangleShape(x + 0.5, y + 0.5, 0.98, 0.98),
                    value.density,
                    value.friction
                )
            end
        end
    end

    return part_entity, block_entities
end

-- Build the machine from all the given parts
local function build_machine(ox, oy, parts, tileset)
    local entities = {}
    local action_list = {}

    for i = 1, #parts do
        local part = parts[i]

        local part_entity, block_entities = build_machine_part(ox, oy, part, action_list, tileset)

        -- add the part (physic body)
        table.insert(entities, part_entity)

        -- add all the blocks (sprites)
        for j = 1, #block_entities do
            table.insert(entities, block_entities[j])
        end
    end

    return entities, action_list
end

--- Flood-fill algorithm to find all blocks connected to the specified one and set them to nil
--
-- This function will return a list containing all the blocks connected to the block at the
-- specified coordinates in the grid. Those blocks are also removed from the grid.
local function remove_connected_blocks(cols, rows, data, x, y)
    -- list in which we're gonna put all the connected blocks
    local blocks = {}

    -- immediately return if we're outside of the grid, or if we're on an empty block (nothing)
    if x < 1 or x > cols or y < 1 or y > rows or not data[x][y] then
        return blocks
    end

    -- get the value of the block
    local value = data[x][y]

    -- remove it from the grid
    data[x][y] = nil

    -- add this first block to the list (we are connected to ourselves)
    table.insert(blocks, {
        value = value;
        x = x - 1;
        y = y - 1;
    })

    -- sides on which the block can connect to other blocks
    -- the order is:
    --   [1] = UP
    --   [2] = RIGHT
    --   [3] = DOWN
    --   [4] = LEFT
    -- the default value is { true, true, true, true }, meaning it can connect in any direction
    local fix = value.fix or { true, true, true, true }

    -- now we connect to blocks on the sides

    -- up
    if
        -- check that we can fix on this side
        fix[1] and
        -- check that we aint on the border
        y > 1 and
        -- check that there's something on this side
        data[x][y - 1] ~= nil and
        -- check that this thing can connect to us (it can "fix" on the opposite direction: down)
        (data[x][y - 1].fix == nil or data[x][y - 1].fix[3])
    then
        -- remove all the blocks connected to this neighbour (recursive)
        local n_blocks = remove_connected_blocks(cols, rows, data, x, y - 1)

        -- add all the blocks connected to it to our list
        -- => the blocks connected to our neighbour block are also connected to us
        for i = 1, #n_blocks do
            table.insert(blocks, n_blocks[i])
        end
    end

    -- right
    if
        -- check that we can fix on this side
        fix[2] and
        -- check that we aint on the border
        x < cols and
        -- check that there's something on this side
        data[x + 1][y] ~= nil and
        -- check that this thing can connect to us (it can "fix" on the opposite direction: left)
        (data[x + 1][y].fix == nil or data[x + 1][y].fix[4])
    then
        -- remove all the blocks connected to this neighbour (recursive)
        local e_blocks = remove_connected_blocks(cols, rows, data, x + 1, y)

        -- add all the blocks connected to it to our list
        -- => the blocks connected to our neighbour block are also connected to us
        for i = 1, #e_blocks do
            table.insert(blocks, e_blocks[i])
        end
    end

    -- down
    if
        -- check that we can fix on this side
        fix[3] and
        -- check that we aint on the border
        y < rows and
        -- check that there's something on this side
        data[x][y + 1] ~= nil and
        -- check that this thing can connect to us (it can "fix" on the opposite direction: up)
        (data[x][y + 1].fix == nil or data[x][y + 1].fix[1])
    then
        -- remove all the blocks connected to this neighbour (recursive)
        local s_blocks = remove_connected_blocks(cols, rows, data, x, y + 1)

        -- add all the blocks connected to it to our list
        -- => the blocks connected to our neighbour block are also connected to us
        for i = 1, #s_blocks do
            table.insert(blocks, s_blocks[i])
        end
    end

    -- left
    if
        -- check that we can fix on this side
        fix[4] and
        -- check that we aint on the border
        x > 1 and
        -- check that there's something on this side
        data[x - 1][y] ~= nil and
        -- check that this thing can connect to us (it can "fix" on the opposite direction: right)
        (data[x - 1][y].fix == nil or data[x - 1][y].fix[2])
    then
        -- remove all the blocks connected to this neighbour (recursive)
        local w_blocks = remove_connected_blocks(cols, rows, data, x - 1, y)

        -- add all the blocks connected to it to our list
        -- => the blocks connected to our neighbour block are also connected to us
        for i = 1, #w_blocks do
            table.insert(blocks, w_blocks[i])
        end
    end

    -- finally return the block list
    return blocks
end

-- Get all the separate parts that make up the machine
local function get_parts(cols, rows, data)
    local parts = {}

    local datacp = utils.copy(data, true)

    for x = 1, cols do
        for y = 1, rows do
            local id = datacp[x][y]

            if id ~= nil then
                local connected_blocks = remove_connected_blocks(cols, rows, datacp, x, y)

                -- move every block to have the origin be the first one (pretty useless yeah)
                for i = 1, #connected_blocks do
                    local block = connected_blocks[i]

                    block.x = block.x - x + 1
                    block.y = block.y - y + 1
                end

                table.insert(parts, {
                    x = x - 1;
                    y = y - 1;
                    data = connected_blocks;
                })
            end
        end
    end

    return parts
end

function GameplayState:is_simulation_running()
    return self.in_game
end

-- Function to build the vehicle and start the gameplay
function GameplayState:launch_game()
    -- love.audio.play(self.sono)

    if not self.in_game then
        self.in_game = true

        self.world:remove_entity(self.build_grid)

        local grid_comp = self.build_grid:get(BuildGridComponent)
        local xform = self.build_grid:get(Transform)

        local grid_x = xform.x
        local grid_y = xform.y

        local parts = get_parts(grid_comp.columns, grid_comp.rows, grid_comp.data)
        local entities, actions = build_machine(grid_x, grid_y, parts, self.blocks_tileset)

        self.machine_entities = entities
        self.machine_actions = actions
        self.world:add_entities(entities)
    end
end

function GameplayState:stop_game()
    if self.in_game then
        self.in_game = false

        self.world:add_entity(self.build_grid)
        self.world:remove_entities(self.machine_entities)

        self.camera.x = -3
        self.camera.y = -10
        self.machine_entities = nil
        self.machine_actions = nil
    end
end

function GameplayState:reset_grid()
    local grid_comp = self.build_grid:get(BuildGridComponent)

    for x = 1, grid_comp.columns do
        for y = 1, grid_comp.rows do
            grid_comp.data[x][y] = nil
        end
    end
end

return GameplayState

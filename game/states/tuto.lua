local Gamestate = require "hump.gamestate"

local TutoState = {}

function TutoState:init()
    love.graphics.reset()
end

function TutoState:leave()
end

function TutoState:update(dt)

end

function TutoState:mousepressed(x, y, button)

end

function TutoState:draw()

end

return TutoState

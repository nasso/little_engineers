local Settings = require "assets.settings"

local Timer = require "hump.timer"
local Gamestate = require "hump.gamestate"

local IntroState = {}

function IntroState:init()
    self.image = love.graphics.newImage("assets/splashscreen.png")
    self.image:setFilter("nearest", "nearest")
    self.splash_opacity = 0.0
    self.next_state = require("game.states.menu")

    Timer.script(function(wait)
        Timer.tween(3.0, self, { splash_opacity = 1.0 })
        wait(6.0)
        Timer.tween(3.0, self, { splash_opacity = 0.0 })
        wait(3.0)

        Gamestate.switch(self.next_state)
    end)

    love.graphics.reset()
end

function IntroState:leave()
    self.image:release()
end

function IntroState:update(dt)
    Timer.update(dt)
end

function IntroState:keypressed()
    Gamestate.switch(self.next_state)
end

function IntroState:draw()
    local frame_width, frame_height = love.graphics.getDimensions()

    local opacity = self.splash_opacity

    opacity = math.floor(opacity * 32) / 32

    local w, h = self.image:getDimensions()
    local img_scale = math.min(frame_width / w, frame_height / h)
    local img_w = w * img_scale
    local img_h = h * img_scale
    local img_x = (frame_width - img_w) / 2
    local img_y = (frame_height - img_h) / 2

    love.graphics.setColor(1, 1, 1, opacity)
    love.graphics.draw(
        self.image,
        img_x, img_y,
        0,
        img_scale, img_scale
    )
end

return IntroState

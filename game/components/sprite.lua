local args = require "utils.args"
local Component = require "utils.ecs.component"

local Sprite = Component.new()

function Sprite:init(tileset, tileid, x, y, r, sx, sy, ox, oy, kx, ky)
    self.tileset = tileset
    self.t = 0
    self.frame = 1
    self.animated = true
    self.tileid = tileid or 0

    self.x = args.get_or(x, 0.0)
    self.y = args.get_or(y, 0.0)
    self.r = args.get_or(r, 0.0)
    self.sx = args.get_or(sx, 1.0)
    self.sy = args.get_or(sy, self.sx)
    self.ox = args.get_or(ox, 0.0)
    self.oy = args.get_or(oy, 0.0)
    self.kx = args.get_or(kx, 0.0)
    self.ky = args.get_or(ky, 0.0)
end

function Sprite:resetAnimation()
    self.t = 0
    self.frame = 1
end

function Sprite:getCurrentTile()
    local anim = self.tileset:getTileAnimation(self.tileid)

    if anim ~= nil then
        return anim[self.frame].tileid
    else
        return self.tileid
    end
end

function Sprite:getWidth()
    return self.tileset.tile_width
end

function Sprite:getHeight()
    return self.tileset.tile_height
end

return Sprite

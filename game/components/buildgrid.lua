local Component = require "utils.ecs.component"

-- Build grid component
local BuildGridComponent = Component.new()

function BuildGridComponent:init(tileset, columns, rows, margins)
    self.tileset = tileset
    self.columns = columns or 6
    self.rows = rows or 4
    self.margins = margins or 0.05

    self.data = {}

    for i = 1, self.columns do
        self.data[i] = {}
    end
end

return BuildGridComponent

local Component = require "utils.ecs.component"

local args = require "utils.args"

local Terrain = Component.new()

function Terrain:init()
    self.layers = {}
end

function Terrain:addLayer(color, alt)
    table.insert(
        self.layers,

        {
            color = args.get_or(color, { 0.5, 0.5, 0.5 });
            alt = args.get_or(alt, 0.1);
        }
    )

    return self
end

return Terrain

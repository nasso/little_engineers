local Component = require "utils.ecs.component"

-- Camera component
local CameraComponent = Component.new()

function CameraComponent:init(x, y, zoom, rot)
    self.x = x or 0
    self.y = y or 0
    self.zoom = zoom or 1
    self.rot = rot or 0
end

function CameraComponent:world_coords(x, y)
    --[[
        cos(r) / zoom     -sin(r) / zoom      x
        sin(r) / zoom     cos(r) / zoom       y
        0                 0                   1
    ]]

    local cos = math.cos(-self.rot)
    local sin = math.sin(-self.rot)

    return
        self.x + x * cos / self.zoom - y * sin / self.zoom,
        self.y + x * sin / self.zoom + y * cos / self.zoom
end

function CameraComponent:move(x, y)
    self.x = self.x + x
    self.y = self.y + y
end

return CameraComponent

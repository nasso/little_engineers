local Component = require "utils.ecs.component"

-- Mouse input component
local MouseInputComponent = Component.new()

function MouseInputComponent:init(button_count)
    self.old_x = 0
    self.old_y = 0
    self.x = 0
    self.y = 0
    self.dx = 0
    self.dy = 0

    self.wheel_dx = 0
    self.wheel_dy = 0

    self.button_count = button_count or 3
    self.pressed = {}
    self.down = {}
    self.released = {}

    for i = 1, self.button_count do
        self.pressed[i] = false
        self.down[i] = false
        self.released[i] = false
    end
end

return MouseInputComponent

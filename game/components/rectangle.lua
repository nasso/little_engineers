local Component = require "utils.ecs.component"

local RectangleComponent = Component.new()

function RectangleComponent:init(x, y, w, h, fill, stroke, stroke_width)
    self.x = x or 0
    self.y = y or 0
    self.w = w or 1
    self.h = h or 1
    self.fill = fill or { 1, 1, 1, 1 }
    self.stroke = stroke or nil
    self.stroke_width = stroke_width or 0.1
end

return RectangleComponent

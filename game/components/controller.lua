local Component = require "utils.ecs.component"
local Class = require "hump.class"

local Controller = Component.new()

function Controller:init(bindings)
    self.bindings = bindings or {}
end

Controller.Binding = Class {}

function Controller.Binding:init(options)
    self.keys = options.keys or {}
end

function Controller.Binding:__add(o)
    return Controller.Binding({
        keys = { unpack(self.keys), unpack(o.keys) };
    })
end

function Controller.Binding:value()
    if love.keyboard.isScancodeDown(unpack(self.keys)) then return 1.0 end

    return 0.0
end

function Controller.Binding:isDown()
    return self:value() >= 0.5
end

function Controller.Binding:__tostring()
    local size = #self.set

    if size == 0 then
        return "{ <empty> }"
    end

    local str = "{ "

    for i, v in ipairs(self.set) do
        if v.key ~= nil then
            str = str .. "Key<" .. tostring(v.key) .. ">"
        else
            str = str .. "unknown"
        end

        if i < size then
            str = str .. " + "
        end
    end

    return str .. " }"
end

function Controller.Key(...) return Controller.Binding({ keys = { ... } }) end

return Controller

local Component = require "utils.ecs.component"

local ButtonComponent = Component.new()

local function default_action() end

function ButtonComponent:init(x, y, w, h, action)
    self.x = x or 0
    self.y = y or 0
    self.w = w or 1
    self.h = h or 1
    self.action = action or default_action
end

return ButtonComponent

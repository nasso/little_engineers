local args = require "utils.args"
local Component = require "utils.ecs.component"

-- Transform component
local Transform = Component.new()

function Transform:init(x, y, r, sx, sy, ox, oy, kx, ky, parent)
    self.x = args.get_or(x, 0.0)
    self.y = args.get_or(y, 0.0)
    self.r = args.get_or(r, 0.0)
    self.sx = args.get_or(sx, 1.0)
    self.sy = args.get_or(sy, self.sx)
    self.ox = args.get_or(ox, 0.0)
    self.oy = args.get_or(oy, 0.0)
    self.kx = args.get_or(kx, 0.0)
    self.ky = args.get_or(ky, 0.0)
    self.parent = parent
end

-- set the parent and return itself
-- useful when creating a Transform in one line:
--
-- entity:add(Transform():set_parent(parent))
--
function Transform:set_parent(e)
    self.parent = e

    return self
end

function Transform:identity()
    self.x = 0.0
    self.y = 0.0
    self.r = 0.0
    self.sx = 1.0
    self.sy = 1.0
    self.ox = 0.0
    self.oy = 0.0
    self.kx = 0.0
    self.ky = 0.0
end

function Transform:translate(dx, dy)
    self.x = self.x + dx
    self.y = self.y + dy

    return self
end

function Transform:rotate(da)
    self.r = self.r + da

    return self
end

function Transform:scale(sx, sy)
    self.sx = self.sx * sx
    self.sy = self.sy * args.get_or(sy, sx)

    return self
end

return Transform

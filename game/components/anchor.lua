local Component = require "utils.ecs.component"

local Anchor = Component.new()

function Anchor:init(horizontal, vertical, rect)
    self.horizontal = horizontal or "left"
    self.vertical = vertical or "top"
    self.rect = rect or nil
end

return Anchor

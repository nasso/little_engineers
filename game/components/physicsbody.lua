local Class = require "hump.class"
local Component = require "utils.ecs.component"

local PhysicsBody = Component.new()

PhysicsBody.ChainShape = Class {}
function PhysicsBody.ChainShape:init(loop, points)
    self.type = "chain"
    self.loop = loop
    self.points = points
end

PhysicsBody.CircleShape = Class {}
function PhysicsBody.CircleShape:init(radius, x, y)
    self.type = "circle"
    self.radius = radius
    self.x = x or 0
    self.y = y or 0
end

PhysicsBody.EdgeShape = Class {}
function PhysicsBody.EdgeShape:init(x1, y1, x2, y2)
    self.type = "edge"
    self.x1 = x1
    self.y1 = y1
    self.x2 = x2
    self.y2 = y2
end

PhysicsBody.PolygonShape = Class {}
function PhysicsBody.PolygonShape:init(points)
    self.type = "polygon"
    self.points = points
end

PhysicsBody.RectangleShape = Class {}
function PhysicsBody.RectangleShape:init(x, y, width, height, angle)
    self.type = "rectangle"
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.angle = angle or 0
end

PhysicsBody.Fixture = Class {}
function PhysicsBody.Fixture:init(shape, density, friction)
    self.shape = shape

    self.density = density or 1
    self.friction = friction or 0.8
end

PhysicsBody.TYPE_STATIC = "static"
PhysicsBody.TYPE_DYNAMIC = "dynamic"
PhysicsBody.TYPE_KINEMATIC = "kinematic"

function PhysicsBody.static() return PhysicsBody(PhysicsBody.TYPE_STATIC) end
function PhysicsBody.dynamic() return PhysicsBody(PhysicsBody.TYPE_DYNAMIC) end
function PhysicsBody.kinematic() return PhysicsBody(PhysicsBody.TYPE_KINEMATIC) end

function PhysicsBody:init(btype)
    if
        btype ~= nil and
        btype ~= PhysicsBody.TYPE_STATIC and
        btype ~= PhysicsBody.TYPE_DYNAMIC and
        btype ~= PhysicsBody.TYPE_KINEMATIC then
        error("'" .. btype .. "' isn't a valid PhysicsBody type")
    end

    self.type = btype or "static"

    self.fixtures = {}
    self.forces = {}
    self.impulses = {}
end

function PhysicsBody:add_fixture(f)
    table.insert(self.fixtures, f)

    return self
end

function PhysicsBody:fix(...)
    return self:add_fixture(PhysicsBody.Fixture(...))
end

return PhysicsBody

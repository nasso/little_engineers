local Component = require "utils.ecs.component"

local SkyComponent = Component.new()

function SkyComponent:init(def)
    if def ~= nil then
        self.color = def.color
    end
end

return SkyComponent

return {
    [0] = {
        sprite_id = 0;
    };

    [1] = {
        sprite_id = 1;
        density = 1;
        friction = 0.8;
        sound = "assets/sons/posebois.mp3";
    };

    [2] = {
        sprite_id = 2;
        density = 2;
        friction = 0.6;
        sound = "assets/sons/metale.mp3";
    };

    [3] = {
        sprite_id = 3;
        sound = "assets/sons/rouepose.mp3";

        --      UP,   RIGHT, DOWN,  LEFT
        fix = { true, false, false, false };

        shapes = {
            {
                type = "circle";
                x = 0.5;
                y = 5 / 7;
                r = 2 / 7;

                density = 2;
                friction = 0.001;
            },

            {
                type = "rectangle";
                x = 3 / 7;
                y = 0;
                w = 1 / 7;
                h = 4 / 7;

                density = 2;
                friction = 0.6;
            },

            {
                type = "rectangle";
                x = 1 / 7;
                y = 0;
                w = 5 / 7;
                h = 1 / 14;

                density = 2;
                friction = 0.6;
            }
        };
    };

    [4] = {
        sprite_id = 4;
        sound = "assets/sons/drill.mp3";

        fix = { true, false, false, false };

        actions = {
            {
                type = "force";

                dy = -200;
            }
        };
    };

    [5] = {
        sprite_id = 5;

        fix = { true, false, false, false };

        actions = {
            {
                type = "impulse";

                dy = -250;
            }
        };
    };

    [6] = {
        sprite_id = 0;

        fix = { false, false, true, false };

        actions = {
            {
                type = "force";

                dy = -75;
            }
        };
    };
}

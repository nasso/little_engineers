local Settings = require "assets.settings"

local Gamestate = require "hump.gamestate"

function love.load()
    love.physics.setMeter(1)

    Gamestate.registerEvents()

    -- Switch to the Intro gamestate!
    Gamestate.switch(require "game.states.intro")
end

function love.keypressed(key, scancode, isrepeat)
    if not isrepeat then
        if scancode == "f3" then
            Settings.debug["transform"] = not Settings.debug["transform"]
        elseif scancode == "f4" then
            Settings.debug["physics"] = not Settings.debug["physics"]
        end
    end
end

function love.resize(w, h)
    Settings.FRAME_WIDTH = w
    Settings.FRAME_HEIGHT = h
end
